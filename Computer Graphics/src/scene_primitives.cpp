#include "scene_primitives.h"

#include "vec2.h"

#include <vector>

//se manda a llamar una vez cuando inicia la aplicacion
void scene_primitives::init() {
	std::vector<cgmath::vec2> positions;
	positions.push_back(cgmath::vec2(0.0f,0.0f));
	positions.push_back(cgmath::vec2(-1.0f, 0.0f));
	positions.push_back(cgmath::vec2(0.0f, -0.4f));
	positions.push_back(cgmath::vec2(1.0f, 0.0f));

	//crear un identificador para un vertex array object
	//guarda el id en vao
	//identificadores
	glGenVertexArrays(1,&vao);
	//empezar a trabajar con el siguiente vao
	glBindVertexArray(vao);

	//crear un identificador para un vertex buffer object, guardar el iden positions VBO
	glGenBuffers(1,&positionsVBO);
	//trabajar con el buffer positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO);
	//crea la memoria del buffer, especifica los datos y la manda al GPU
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);
	//prender el atributo 0
	glEnableVertexAttribArray(0);
	//configurar el atributo 0, numero de componentes, tipo de dato de cada componente
	//normalizamos los datos?
	//desfazamiento entre los atriubutos en la lista
	//apuntador a los datos si no se han mandado
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	//cuando hacemos un bind con 0
	// -> unbind
	//unbind de positionsVBO
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//Unbind del vao
	glBindVertexArray(0);

	primitiveType = GL_POINTS;
}

void scene_primitives::awake() {
	glClearColor(0.0f,0.0f,0.0f,1.0f);
	glPointSize(20.0f);
}

void scene_primitives::sleep() {
	glPointSize(1.0f);
}

void scene_primitives::mainLoop() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	//bind del vao que tiene todos los atributos
	glBindVertexArray(vao);
	//llamada adibujar
	//tipo de primitiva
	//desde que indice se empieza a dibujar, cuantos vertices se dibujan
	//glDrawArrays(GL_POINTS, 0, 4);
	glDrawArrays(primitiveType, 0, 4);
	//Unbind del vaoy todos los atributos
	glBindVertexArray(0);
}

void scene_primitives::normalKeysDown(unsigned char key) {
	if (key == '1') {
		primitiveType = GL_POINTS;
	}

	if (key == '2') {
		primitiveType = GL_LINES;
	}
}