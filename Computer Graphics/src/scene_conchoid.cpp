#include "scene_conchoid.h"
#include "vec2.h"
#include <math.h>

cgmath::vec2 getConcoidUp(cgmath::vec2 p, cgmath::vec2 &o, float k) {
	cgmath::vec2 d = k * d.normalize(o - p);
	return p - d;
}

cgmath::vec2 getConcoidDown(cgmath::vec2 p, cgmath::vec2 &o, float k) {
	cgmath::vec2 d = k * d.normalize(o - p);
	return p + d;
}

static void loadVertex(std::vector<cgmath::vec2> &positions, int i, GLuint vao[], GLuint positionsVBO[]) {
	//glGenVertexArrays(vaoSize, &vao[i]);
	glGenVertexArrays(1, &vao[i]);
	glBindVertexArray(vao[i]);
	//glGenBuffers(vaoSize, &positionsVBO[i]);
	glGenBuffers(1, &positionsVBO[i]);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO[i]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO[i]);
	glBindVertexArray(vao[i]);
}

void scene_conchoid::init() {	
	//getpoints
	float k = 0.1f;
	cgmath::vec2 o(0.0f,0.4f);

	float interval = 0.01f;
	float wsize = 2.0f;

	float range = 1.0f / 16;

	for (int j = 0; j < vaoSize; j++) {
		if (j % 2 == 0) {
			std::vector<cgmath::vec2> positionsUp;
			for (float i = -wsize; i < wsize; i += interval) {
				cgmath::vec2 q = getConcoidUp(cgmath::vec2(i, 0), o, k);
				if (!(q.x > -range && q.y > -range && q.x < range && q.y < range)) {
					positionsUp.push_back(q);
				}
			}

			positionsSize[j] = positionsUp.size();
			loadVertex(positionsUp, j, vao, positionsVBO);
		}
		else {
			std::vector<cgmath::vec2> positionsDown;
			for (float i = -wsize; i < wsize; i += interval) {
				cgmath::vec2 q = getConcoidDown(cgmath::vec2(i, 0), o, k);
				if (!(q.x > -range && q.y > -range && q.x < range && q.y < range)) {
					positionsDown.push_back(q);
				}
			}

			positionsSize[j] = positionsDown.size();
			loadVertex(positionsDown, j, vao, positionsVBO);
			k += 0.1f;
		}
	}

	primitiveType = GL_LINE_STRIP;
}

void scene_conchoid::awake() {
	glClearColor(1.0f, 0.5f, 0.5f, 1.0f);
	glPointSize(1.0f);
}

void scene_conchoid::sleep() {
	glPointSize(1.0f);
}

void scene_conchoid::mainLoop() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (int i = 0; i < vaoSize; i++) {
		glBindVertexArray(vao[i]);
		glDrawArrays(primitiveType, 0, positionsSize[i]);
		glBindVertexArray(vao[i]);
	}
}

void scene_conchoid::normalKeysDown(unsigned char key) {
	GLenum mode[] = {GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY, GL_TRIANGLES_ADJACENCY
};
	if (key == '1') {
		primitiveType = mode[0];
	}

	if (key == '2') {
		primitiveType = mode[1];
	}

	if (key == '3') {
		primitiveType = mode[2];
	}

	if (key == '4') {
		primitiveType = mode[3];
	}

	if (key == '5') {
		primitiveType = mode[4];
	}

	if (key == '6') {
		primitiveType = mode[5];
	}

	if (key == '7') {
		primitiveType = mode[6];
	}

	if (key == '8') {
		primitiveType = mode[7];
	}

	if (key == '9') {
		primitiveType = mode[8];
	}

	if (key == '0') {
		primitiveType = mode[9];
	}
}