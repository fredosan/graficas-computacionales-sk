﻿#include "final_scene.h"

#define RADIUS 3.0
#define PI 3.141519

/*inspiration sources:

https://flafla2.github.io/2014/08/09/perlinnoise.html
https://thebookofshaders.com/11/
https://solarianprogrammer.com/2012/07/18/perlin-noise-cpp-11/
https://forum.processing.org/two/discussion/26718/how-to-correctly-apply-perlin-noise-to-a-sphere

*/

//chaikin
static cgmath::mat3 getForthPoints(cgmath::vec3 &pi, cgmath::vec3 &pii) {
	cgmath::vec3 direction = cgmath::vec3().normalize(pii - pi);
	cgmath::vec3 qi = (3.0 / 4.0)*pi + (1.0 / 4.0)*pii;
	cgmath::vec3 ri = (1.0 / 4.0)*pi + (3.0 / 4.0)*pii;
	return cgmath::mat3(qi, ri, direction);
}

static void getCurvedPoints(std::vector<cgmath::vec2> &controlPoints, std::vector<cgmath::vec2> &curvedPoints, int ref, int loop) {
	std::vector<cgmath::vec2> subControlPoints = controlPoints;
	for (int i = 0; i < ref; i++) {
		cgmath::vec3 pi;
		cgmath::vec3 pii;

		std::vector<cgmath::vec2> tmpControlPoints;
		int onetime = 1;
		for (std::vector<cgmath::vec2>::iterator it = subControlPoints.begin(); it != subControlPoints.end(); ++it) {
			pi = cgmath::vec3(*it, 0.0);
			if (*it == subControlPoints.back()) {
				if (loop) {
					pii = cgmath::vec3(*subControlPoints.begin(), 0.0);
				}
				else {
					pii = pi;
				}
			}
			else {
				pii = cgmath::vec3(*std::next(it), 0.0);
			}
			cgmath::mat3 newMatrix = getForthPoints(pi, pii);
			if (onetime && !loop) {
				tmpControlPoints.push_back(cgmath::vec2(pi.x, pi.y));
				onetime = 0;
			}
			tmpControlPoints.push_back(cgmath::vec2(newMatrix[0].x, newMatrix[0].y));
			tmpControlPoints.push_back(cgmath::vec2(newMatrix[1].x, newMatrix[1].y));
		}
		subControlPoints.clear();
		subControlPoints = tmpControlPoints;
		tmpControlPoints.clear();

		if (onetime && !loop) {
			subControlPoints.erase(subControlPoints.begin());
			subControlPoints.pop_back();
		}
	}
	curvedPoints = subControlPoints;
}

//camera route
static void cameraRoute(std::vector<cgmath::vec2> &chaikinCtr, std::vector<cgmath::vec2> &cameraRoute) {
	//set chaikin control points
	//float factor = 2.0f;
	//for (float i = 0.0f; i < 2 * PI; i += (float)(PI / 2)) {
	//	cgmath::vec2 r = cgmath::vec2((float)(RADIUS+factor)*sin(i), (float)(RADIUS+factor)*cos(i));
	//	r += sin(i)*r;
	//	chaikinCtr.push_back(r);
	//}

	chaikinCtr.push_back(cgmath::vec2(0.0f, 10.0f));
	chaikinCtr.push_back(cgmath::vec2(-2.0f, 20.0f));
	chaikinCtr.push_back(cgmath::vec2(2.0f, 30.0f));
	chaikinCtr.push_back(cgmath::vec2(-2.0f, 40.0f));
	chaikinCtr.push_back(cgmath::vec2(4.0f, 30.0f));
	chaikinCtr.push_back(cgmath::vec2(-4.0f, 20.0f));
	chaikinCtr.push_back(cgmath::vec2(0.0f, 10.0f));

	//set curved route for camera
	getCurvedPoints(chaikinCtr, cameraRoute, 10, 1);
}

static void shadersLoader(GLuint &shader_program, const char * vertexShader, const char * fragmentShader, unsigned int terrain) {
	ifile shader_file;
	shader_file.read(vertexShader);
	std::string vertex_source = shader_file.get_contents();
	const GLchar* vertex_source_c = (const GLchar*)vertex_source.c_str();
	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	glShaderSource(vertex_shader, 1, &vertex_source_c, nullptr);
	// Compilamos el codigo fuente contenido en el shader
	// con identificador vertex_shader.
	glCompileShader(vertex_shader);

	// Revision de errores de compilacion del vertex shader
	GLint vertex_compiled;
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &vertex_compiled);
	if (vertex_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(vertex_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(vertex_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in vertex shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	shader_file.read(fragmentShader);
	std::string fragment_source = shader_file.get_contents();
	const GLchar* fragment_source_c = (const GLchar*)fragment_source.c_str();

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	glShaderSource(fragment_shader, 1, &fragment_source_c, nullptr);
	glCompileShader(fragment_shader);

	// Revision de errores de compilacion del fragment shader
	GLint fragment_compiled;
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &fragment_compiled);
	if (fragment_compiled != GL_TRUE)
	{
		GLint log_length;
		glGetShaderiv(fragment_shader, GL_INFO_LOG_LENGTH, &log_length);

		std::vector<GLchar> log;
		log.resize(log_length);
		glGetShaderInfoLog(fragment_shader, log_length, &log_length, &log[0]);
		std::cout << "Syntax errors in fragment shader: " << std::endl;
		for (auto& c : log) std::cout << c;
		std::cout << std::endl;
	}

	shader_program = glCreateProgram();
	glAttachShader(shader_program, vertex_shader);
	glAttachShader(shader_program, fragment_shader);

	if (terrain) {
		glBindAttribLocation(shader_program, 0, "VertexPosition");
		//glBindAttribLocation(shader_program, 1, "VertexCoord");
		glBindAttribLocation(shader_program, 1, "VertexColor");
		glBindAttribLocation(shader_program, 2, "VertexNormal");
	}
	else {
		glBindAttribLocation(shader_program, 3, "WaterPosition");
		//glBindAttribLocation(shader_program, 1, "VertexCoord");
		glBindAttribLocation(shader_program, 4, "WaterColor");
		glBindAttribLocation(shader_program, 5, "WaterNormal");
	}

	glLinkProgram(shader_program);

	glDeleteShader(vertex_shader);
	glDeleteShader(fragment_shader);

	glUseProgram(shader_program);

	//send data to shader
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);

	GLuint lightColor = glGetUniformLocation(shader_program, "LightColor");
	glUniform3f(lightColor, 1.0f, 1.0f, 1.0f);

	GLuint lightPosition = glGetUniformLocation(shader_program, "LightPosition");
	glUniform3f(lightPosition, 50.0f, 0.0f, 50.0f);

	glUseProgram(0);
}

template <typename T>
static void attvboConf(GLuint &vbo, std::vector<T> &positions, unsigned int i, unsigned int typeDraw) {
	//opengl attibute vbo Configuration and Creation
	glGenBuffers(1, &vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vbo);
	if (typeDraw) {
		glBufferData(GL_ARRAY_BUFFER, sizeof(T) * positions.size(), positions.data(), GL_STATIC_DRAW);
		glEnableVertexAttribArray(i);//atributos
		glVertexAttribPointer(i, 3, GL_FLOAT, GL_FALSE, 0, nullptr);//jasd, vec2, tipo float ...
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
	else {
		glBufferData(GL_ARRAY_BUFFER, sizeof(T) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);
		glEnableVertexAttribArray(i);//atributos
		glVertexAttribPointer(i, 2, GL_FLOAT, GL_FALSE, 0, nullptr);//jasd, vec2, tipo float ...
		glBindBuffer(GL_ARRAY_BUFFER, 0);
	}
}

static int permutation[] = { 151,160,137,91,90,15,131,13,201,95,96,53,194,233,7,225,140,36,103,30,69,142,8,99,37,240,21,10,23,
	190, 6,148,247,120,234,75,0,26,197,62,94,252,219,203,117,35,11,32,57,177,33,
	88,237,149,56,87,174,20,125,136,171,168, 68,175,74,165,71,134,139,48,27,166,
	77,146,158,231,83,111,229,122,60,211,133,230,220,105,92,41,55,46,245,40,244,
	102,143,54, 65,25,63,161, 1,216,80,73,209,76,132,187,208, 89,18,169,200,196,
	135,130,116,188,159,86,164,100,109,198,173,186, 3,64,52,217,226,250,124,123,
	5,202,38,147,118,126,255,82,85,212,207,206,59,227,47,16,58,17,182,189,28,42,
	223,183,170,213,119,248,152, 2,44,154,163, 70,221,153,101,155,167, 43,172,9,
	129,22,39,253, 19,98,108,110,79,113,224,232,178,185, 112,104,218,246,97,228,
	251,34,242,193,238,210,144,12,191,179,162,241, 81,51,145,235,249,14,239,107,
	49,192,214, 31,181,199,106,157,184, 84,204,176,115,121,50,45,127, 4,150,254,
	138,236,205,93,222,114,67,29,24,72,243,141,128,195,78,66,215,61,156,180
};

static float fade(float t) {
	return t * t * t * (t * (t * 6 - 15) + 10);
}

static float grad(int hash, float x, float y, float z)
{
	switch (hash & 0xF)
	{
	case 0x0: return  x + y;
	case 0x1: return -x + y;
	case 0x2: return  x - y;
	case 0x3: return -x - y;
	case 0x4: return  x + z;
	case 0x5: return -x + z;
	case 0x6: return  x - z;
	case 0x7: return -x - z;
	case 0x8: return  y + z;
	case 0x9: return -y + z;
	case 0xA: return  y - z;
	case 0xB: return -y - z;
	case 0xC: return  y + x;
	case 0xD: return -y + z;
	case 0xE: return  y - x;
	case 0xF: return -y - z;
	default: return 0;
	}
}

static float lerp(float a0, float a1, float w) {
	//return (1.0f - w)*a0 + w * a1;
	return a0 + w * (a1 - a0);
}

static float noise(float x, float y, float z, int * p) {
	int xi = (int)x & 255;
	int yi = (int)y & 255;
	int zi = (int)z & 255;
	float xf = x - (int)x;
	float yf = y - (int)y;
	float zf = z - (int)z;
	float u = fade(xf);
	float v = fade(yf);
	float w = fade(zf);

	int aaa, aba, aab, abb, baa, bba, bab, bbb;
	aaa = p[p[p[xi] + yi] + zi];
	aba = p[p[p[xi] + yi++] + zi];
	aab = p[p[p[xi] + yi] + zi++];
	abb = p[p[p[xi] + yi++] + zi++];
	baa = p[p[p[xi++] + yi] + zi];
	bba = p[p[p[xi++] + yi++] + zi];
	bab = p[p[p[xi++] + yi] + zi++];
	bbb = p[p[p[xi++] + yi++] + zi++];

	float x1, x2, y1, y2;
	x1 = lerp(grad(aaa, xf, yf, zf), grad(baa, xf - 1, yf, zf), u);
	x2 = lerp(grad(aba, xf, yf - 1, zf), grad(bba, xf - 1, yf - 1, zf), u);
	y1 = lerp(x1, x2, v);

	x1 = lerp(grad(aab, xf, yf, zf - 1), grad(bab, xf - 1, yf, zf - 1), u);
	x2 = lerp(grad(abb, xf, yf - 1, zf - 1), grad(bbb, xf - 1, yf - 1, zf - 1), u);
	y2 = lerp(x1, x2, v);

	return (lerp(y1, y2, w) + 7) / 3;
}

float dotGridGradient(int ix, int iy, float x, float y) {
	float dx = x - (float)ix;
	float dy = y - (float)iy;
	float factor = 2.0f;
	return (dx * (((float)rand() / (RAND_MAX)) + 1)*factor + dy * (((float)rand() / (RAND_MAX)) + 1)*factor);
}

static float perlinNoise(float x, float y) {
	int x0 = (int)x;
	int x1 = x0 + 1;
	int y0 = (int)y;
	int y1 = y0 + 1;

	float sx = x - (float)x0;
	float sy = y - (float)y0;

	float n0, n1, ix0, ix1, value;

	n0 = dotGridGradient(x0, y0, x, y);
	n1 = dotGridGradient(x1, y0, x, y);
	ix0 = lerp(n0, n1, sx);

	n0 = dotGridGradient(x0, y1, x, y);
	n1 = dotGridGradient(x1, y1, x, y);
	ix1 = lerp(n0, n1, sx);

	value = lerp(ix0, ix1, sy);
	return value;
}

static void sphere_generation(std::vector<cgmath::vec3> &sp, std::vector<cgmath::vec3> &n, std::vector<cgmath::vec2> &tc, std::vector<cgmath::vec3> sea, std::vector<int> &indices, int &indicesCount, unsigned int terrain, int * pp) {
	float x, y, z, xy;
	float radius = RADIUS;
	float nx, ny, nz, lengthInv = 1.0f / radius;
	float s, t;

	//72 & 24
	int sectorCount = 200;
	int stackCount = 100;

	float sectorStep = 2.0f * (float)PI / sectorCount;
	float stackStep = (float)PI / stackCount;
	float sectorAngle, stackAngle;

	for (int i = 0; i <= stackCount; i++) {
		stackAngle = (float)PI / 2 - i * stackStep;

		xy = radius * cosf(stackAngle);
		z = radius * sinf(stackAngle);

		for (int j = 0; j <= sectorCount; j++) {
			sectorAngle = j * sectorStep;
			x = xy * cosf(sectorAngle);
			y = xy * sinf(sectorAngle);

			cgmath::vec3 p(x, y, z);
			//terrain generation?
			if (terrain) {
				p.normalize();
				//float r = noise(p.x, p.y, p.z, pp);
				int off = 4;// ((rand() % 8) + 1);
				float r = noise(abs(p.x*off), abs(p.y*off), abs(p.z*off), pp);
				float rr = noise(p.x, p.y, p.z, pp);
				p -= (p*(rr / 1000));

				p += (p*r);

				s = (float)j / sectorCount;
				t = (float)i / stackCount;
				tc.push_back(cgmath::vec2(s, t));
			}
			else {
				s = (float)j / sectorCount;
				t = (float)i / stackCount;
				sea.push_back(cgmath::vec3(0.0f, 0.0f, cos(s+t)));
			}

			sp.push_back(p);

			nx = p.x * lengthInv;
			ny = p.y * lengthInv;
			nz = p.z * lengthInv;
			cgmath::vec3 pn(nx, ny, nz);
			n.push_back(pn);
		}
	}

	int k1, k2;
	for (int i = 0; i < stackCount; i++) {
		k1 = i * (sectorCount + 1);
		k2 = k1 + sectorCount + 1;

		for (int j = 0; j < sectorCount; j++, k1++, k2++) {
			if (i != 0) {
				indices.push_back(k1);
				indices.push_back(k2);
				indices.push_back(k1 + 1);
				indicesCount += 3;
			}
			if (i != (stackCount - 1)) {
				indices.push_back(k1 + 1);
				indices.push_back(k2);
				indices.push_back(k2 + 1);
				indicesCount += 3;
			}
		}
	}
}

static cgmath::mat4 rotateXY(float thetaZ){
	return cgmath::mat4(cgmath::vec4(cos(thetaZ), sin(thetaZ), 0, 0), cgmath::vec4(-sin(thetaZ), cos(thetaZ), 0, 0), cgmath::vec4(0, 0, 1, 0), cgmath::vec4(0, 0, 0, 1));
}

static cgmath::mat4 rotateXZ(float thetaY) {
	return cgmath::mat4(cgmath::vec4(cos(thetaY), 0, -sin(thetaY), 0), cgmath::vec4(0, 1, 0, 0), cgmath::vec4(sin(thetaY), 0, cos(thetaY), 0), cgmath::vec4(0, 0, 0, 1));
}

static cgmath::mat4 rotateYZ(float thetaX) {
	return cgmath::mat4(cgmath::vec4(1, 0, 0, 0), cgmath::vec4(0, cos(thetaX), sin(thetaX), 0), cgmath::vec4(0, -sin(thetaX), cos(thetaX), 0), cgmath::vec4(0, 0, 0, 1));
}

static cgmath::mat4 getModelMatrix(float thetaX, float thetaY, float thetaZ, float sx, float sy, float sz, float vx, float vy, float vz) {
	cgmath::mat4 rotationMatrixXY(cgmath::vec4(cos(thetaZ), sin(thetaZ), 0, 0), cgmath::vec4(-sin(thetaZ), cos(thetaZ), 0, 0), cgmath::vec4(0, 0, 1, 0), cgmath::vec4(0, 0, 0, 1));
	cgmath::mat4 rotationMatrixXZ(cgmath::vec4(cos(thetaY), 0, -sin(thetaY), 0), cgmath::vec4(0, 1, 0, 0), cgmath::vec4(sin(thetaY), 0, cos(thetaY), 0), cgmath::vec4(0, 0, 0, 1));
	cgmath::mat4 rotationMatrixYZ(cgmath::vec4(1, 0, 0, 0), cgmath::vec4(0, cos(thetaX), sin(thetaX), 0), cgmath::vec4(0, -sin(thetaX), cos(thetaX), 0), cgmath::vec4(0, 0, 0, 1));
	cgmath::mat4 scaleMatrix(cgmath::vec4(sx, 0, 0, 0), cgmath::vec4(0, sy, 0, 0), cgmath::vec4(0, 0, sz, 0), cgmath::vec4(0, 0, 0, 1));
	cgmath::mat4 translationMatrix(cgmath::vec4(sx, 0, 0, 0), cgmath::vec4(0, sy, 0, 0), cgmath::vec4(0, 0, sz, 0), cgmath::vec4(0, 0, 0, 1));
	return rotationMatrixXY * rotationMatrixYZ * rotationMatrixXZ * scaleMatrix * translationMatrix;
}

//perspective
static cgmath::mat4 getProjectionMatrix(float aspect, float fov, float far, float near) {
	return cgmath::mat4(
		cgmath::vec4(1 / (aspect*tan(fov / 2)), 0, 0, 0),
		cgmath::vec4(0, 1 / tan(fov / 2), 0, 0),
		cgmath::vec4(0, 0, -(far + near) / (far - near), -1),
		cgmath::vec4(0, 0, -(2 * far*near) / (far - near), 0));
}

final_scene::~final_scene()
{
	// Borramos la memoria del ejecutable cuando
	// la escena deja de existir.
	engine->drop();
	glDeleteProgram(shader_program);
	glDeleteTextures(1, &textureId);
}


void final_scene::init()
{
	//IrrKlang Configuration
	if (!engine) { std::cout << "Error creating sound engine.\n"; }
	engine->play2D("audio/1.mp3", true);


	srand((int)time::elapsed_time().count());
	//loading Texture
	ilGenImages(1, &imageID);
	ilBindImage(imageID);
	ilLoadImage("img/terrain.png");

	//...some logic step 3
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_2D, textureId);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexImage2D(GL_TEXTURE_2D, 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_WIDTH), ilGetInteger(IL_IMAGE_HEIGHT), 0, ilGetInteger(IL_IMAGE_FORMAT), ilGetInteger(IL_IMAGE_TYPE), ilGetData());

	ilBindImage(0);
	ilDeleteImages(1, &imageID);

	std::vector<cgmath::vec3> spherePositions;
	std::vector<cgmath::vec3> waterPositions;
	std::vector<cgmath::vec3> normals;
	std::vector<cgmath::vec3> waterNormals;
	std::vector<cgmath::vec2> texturePositions;
	std::vector<cgmath::vec3> blue;//color
	
	std::vector<int> indices;
	std::vector<int> iWater;

	//camera route
	cameraRoute(chaikinCtr, cameraRoutePoints);

	for (int x = 0; x < 512; x++) {
		//p[x] = permutation[x % 256];
		p[x] = permutation[x % (rand() % 256 + 1)];
	}

	//Sphere Generation
	sphere_generation(spherePositions, normals, texturePositions, blue, indices, indicesCount, 1, p);

	//Water Generation
	sphere_generation(waterPositions, waterNormals, texturePositions, blue, iWater, iWaterCount, 0, p);

	// Creacion y activacion del vao
	//glGenVertexArrays(1, &vao);
	//glBindVertexArray(vao);

	glGenVertexArrays(1, &vao[0]);
	glBindVertexArray(vao[0]);

	glGenVertexArrays(1, &vao[1]);
	glBindVertexArray(vao[1]);

	// Creacion y configuracion del buffer del atributo de posicion
	attvboConf(positionsVBO, spherePositions, 0, 1);
	//attvboConf(colorsVBO, colors, 1);

	//textures buffers
	attvboConf(textureBuffer, texturePositions, 1, 0);

	//normals buffer
	attvboConf(normalsVBO, normals, 2, 1);

	//water buffer normals & color
	attvboConf(waterVBO, waterPositions, 3, 1);
	attvboConf(colorsVBO, blue, 4, 0);
	attvboConf(waterNormalsVBO, waterNormals, 5, 1);

	glGenBuffers(1, &indicesBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * indices.size(), indices.data(), GL_STATIC_DRAW);

	glGenBuffers(1, &iWaterBuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, iWaterBuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(unsigned int) * iWater.size(), iWater.data(), GL_STATIC_DRAW);

	glBindVertexArray(0);
	glBindVertexArray(1);
	glBindVertexArray(2);
	glBindVertexArray(3);
	glBindVertexArray(4);
	glBindVertexArray(5);

	//shader loader
	shadersLoader(shader_program,"shaders/final_scene.vert","shaders/final_scene.frag",1);
}

void final_scene::awake()
{
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glEnable(GL_PROGRAM_POINT_SIZE);
}

void final_scene::sleep()
{
	glClearColor(1.0f, 1.0f, 0.5f, 1.0f);
	glDisable(GL_PROGRAM_POINT_SIZE);
}

void final_scene::mainLoop()
{
	//Building matrix
	float thetaX = 0.0f;
	float thetaY = 0.0f;
	float thetaZ = 15.0f;

	float t = time::elapsed_time().count();

	float pi = 3.14159f;

	//thetaX = t * (30.0f * (pi / 200.0f));
	//thetaZ = t * (30.0f * (pi / 200.0f));
	//thetaY = t * (30.0f * (pi / 200.0f));
	thetaY = t * (planetRot * (pi / 200.0f));

	float sx = 1.0f;
	float sy = 1.0f;
	float sz = 1.0f;
	float vx = 0.0f;
	float vy = 0.0f;
	float vz = 0.0f;

	cgmath::mat4 modelCameraMatrix(1.0f);

	//modelCameraMatrix[0][3] = -mx;
	//modelCameraMatrix[1][3] = -my;
	//modelCameraMatrix[2][3] = -mz;

	if (animation) {
		float t = time::elapsed_time().count();
		int indexOne = ((100*(int)t) % cameraRoutePoints.size());
		int indexTwo = indexOne - 1;

		if (indexTwo < 0) {
			indexTwo = cameraRoutePoints.size();
		}

		mz = cameraRoutePoints[indexOne].y + (cameraRoutePoints[indexOne].y - cameraRoutePoints[indexTwo].y)*cos(t);
		mx = cameraRoutePoints[indexOne].x + (cameraRoutePoints[indexOne].x - cameraRoutePoints[indexTwo].x)*sin(t);
		my = 0.0f;
	}

	modelCameraMatrix[3] = cgmath::vec4(mx, my, mz, 1.0f);//camera position
	//modelCameraMatrix[3] = cgmath::vec4(0.0f,0.0f,10.0f, 1.0f);//camera position

	//model matrix
	cgmath::mat4 modelMatrix = getModelMatrix(thetaX, thetaY, thetaZ, sx, sy, sz, vx, vy, vz);
	cgmath::mat4 viewMatrix = cgmath::mat4::inverse(modelCameraMatrix);

	cgmath::mat3 normalMatrix = cgmath::mat3::transpose(cgmath::mat3::inverse(modelMatrix.matrixij(modelMatrix,3,3)));

	float aspect = swidth / sheight;
	float fov = 60 * (pi / 200.0f);
	float far = 1000.0f;
	float near = 1.0f;
	cgmath::mat4 projectionMatrix = getProjectionMatrix(aspect, fov, far, near);

	//mvp matrix uniform to shaders
	cgmath::mat4 mvp = projectionMatrix * viewMatrix * modelMatrix;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//terrain
	glUseProgram(shader_program);
	GLuint mvpm = glGetUniformLocation(shader_program, "mvp");
	glUniformMatrix4fv(mvpm, 1, GL_FALSE, &mvp[0][0]);

	GLuint modelMtx = glGetUniformLocation(shader_program, "modelMatrix");
	glUniformMatrix4fv(modelMtx, 1, GL_FALSE, &modelMatrix[0][0]);

	GLuint normalMtx = glGetUniformLocation(shader_program, "normalMatrix");
	glUniformMatrix3fv(normalMtx, 1, GL_FALSE, &normalMatrix[0][0]);

	GLuint cameraPos = glGetUniformLocation(shader_program, "CameraPosition");
	glUniform3f(cameraPos, mx, my, mz);

	//debug--------------------
	//std::cout << "( " << mx << ", " << my << ", " << mz << " )\n";
	//debug--------------------

	GLuint resolution_location = glGetUniformLocation(shader_program, "resolution");
	glUniform2f(resolution_location, 400.0f, 400.0f);
	GLuint time_location = glGetUniformLocation(shader_program, "time");
	glUniform1f(time_location, time::elapsed_time().count());
	glBindVertexArray(vao[0]);
	glBindVertexArray(vao[1]);

	//textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, textureId);

	GLuint tex_location = glGetUniformLocation(shader_program, "textures");
	glUniform1i(tex_location, 0);

	//glDrawArrays(GL_POINTS, 0, 100);//cuando no hay indices
	glDrawElements(GL_TRIANGLES, indicesCount, GL_UNSIGNED_INT, nullptr);
	glDrawElements(GL_TRIANGLES, iWaterCount, GL_UNSIGNED_INT, nullptr);

	//textures
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, 0);

	glBindVertexArray(0);
	glBindVertexArray(1);
	glBindVertexArray(2);
	glBindVertexArray(3);
	glBindVertexArray(4);
	glBindVertexArray(5);
	glUseProgram(0);
}

void final_scene::resize(int width, int height)
{
	swidth = float(width);
	sheight = float(height);
	glViewport(0, 0, width, height);
	glUseProgram(shader_program);
	GLuint resolution_location = glGetUniformLocation(shader_program, "iResolution");
	glUniform2f(resolution_location, GLfloat(width), GLfloat(height));
	glUseProgram(0);
}

void final_scene::normalKeysDown(unsigned char key)
{
	switch (key) {
		case 'w':
			mz -= step;
		break;
		case 's':
			mz += step;
		break;
		case 'a':
			mx -= step;
		break;
		case 'd':
			mx += step;
		break;
		case 'o':
			my += step;
		break;
		case 'l':
			my -= step;
		break;
		case 'e':
			planetRot += step;
		break;
		case 'q':
			planetRot -= step;
		break;
		case 'z':
			planetRot = 0.0f;
		break;
		case 't':
			animation = 0;
		break;
	}
}
