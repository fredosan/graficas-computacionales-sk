#include <vector>
//#include <iostream>
#include "sphere.h"
#include "hitable_list.h"
#include "camera.h"
#include "material.h"
#include "image_writer.h"

/*Random float inspiration https://stackoverflow.com/questions/686353/random-float-number-generation*/

vec3 color(const ray& r, hitable *world, int depth) {
	hit_record rec;
	if (world->hit(r, 0.001, FLT_MAX, rec)) {
		ray scattered;
		vec3 attenuation;
		//material *mat = rec.mat_ptr->scatter(r, rec, attenuation, scattered);
		if (depth < 50 && rec.mat_ptr->scatter(r, rec, attenuation, scattered)) {
			return attenuation * color(scattered,world,depth+1);
		}
		else {
			return vec3(0,0,0);
		}
	}
	else {
		vec3 unit_direction = unit_vector(r.direction());
		float t = 0.5*(unit_direction.y() + 1.0);
		return (1.0 - t)*vec3(1.0, 1.0, 1.0) + t * vec3(0.5, 0.7, 1.0);
	}
}

hitable *random_scene() {
	int n = 660;
	hitable **list = new hitable*[n + 1];
	list[0] = new sphere(vec3(0, -1000, 0), 1000, new lambertian(vec3(0.5, 0.5, 0.5)));
	int i = 1;
	for (int a = -11; a < 11; a++) {
		for (int b = -11; b < 11; b++) {
			float choose_mat = ((double)rand() / (RAND_MAX));
			vec3 center(a + 0.9*((double)rand() / (RAND_MAX)), 0.2, b + 0.9*((double)rand() / (RAND_MAX)));
			if ((center - vec3(4, 0.2, 0)).length() > 0.9) {
				if (choose_mat < 0.8) {  // diffuse
					list[i++] = new sphere(center, 0.2, new lambertian(vec3(((double)rand() / (RAND_MAX))*((double)rand() / (RAND_MAX)), ((double)rand() / (RAND_MAX))*((double)rand() / (RAND_MAX)), ((double)rand() / (RAND_MAX))*((double)rand() / (RAND_MAX)))));
				}
				else if (choose_mat < 0.95) { // metal
					list[i++] = new sphere(center, 0.2,
						new metal(vec3(0.5*(1 + ((double)rand() / (RAND_MAX))), 0.5*(1 + ((double)rand() / (RAND_MAX))), 0.5*(1 + ((double)rand() / (RAND_MAX)))), 0.5*((double)rand() / (RAND_MAX))));
				}
				else {  // glass
					list[i++] = new sphere(center, 0.2, new dielectric(1.5));
				}
			}
		}
	}

	/*list[i++] = new sphere(vec3(0, 1, 0), 1.0, new dielectric(1.5));
	list[i++] = new sphere(vec3(-4, 1, 0), 1.0, new dielectric(1.0));
	list[i++] = new sphere(vec3(4, 1, 0), 1.0, new dielectric(0.5));
	list[i++] = new sphere(vec3(8, 1, 0), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
	list[i++] = new sphere(vec3(-8, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));*/

	list[i++] = new sphere(vec3(0, 1, 0), 1.0, new dielectric(1.5));
	list[i++] = new sphere(vec3(2, 1, 2), 1.0, new dielectric(1.0));
	list[i++] = new sphere(vec3(-2, 1, -2), 1.0, new dielectric(0.5));
	list[i++] = new sphere(vec3(4, 1, 4), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
	list[i++] = new sphere(vec3(-4, 1, -4), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));

	return new hitable_list(list, i);
}

int main() {
	int nx = 1200;
	int ny = 800;
	int ns = 100;

	std::vector<unsigned char> pixels;
	/*
	float R = cos(3.1415927 / 4);
	hitable *list[5];
	list[0] = new sphere(vec3(-R, 0, -1), R, new lambertian(vec3(0, 0, 1)));
	list[1] = new sphere(vec3(R, 0, -1), R, new lambertian(vec3(1, 0, 0)));
	hitable *world = new hitable_list(list, 2);*/
	
	/*hitable *list[10];
	list[0] = new sphere(vec3(0, 1, 0), 1.0, new dielectric(1.5));
	list[1] = new sphere(vec3(-4, 1, 0), 1.0, new dielectric(1.0));
	list[2] = new sphere(vec3(4, 1, 0), 1.0, new dielectric(0.5));
	list[3] = new sphere(vec3(8, 1, 0), 1.0, new lambertian(vec3(0.4, 0.2, 0.1)));
	list[4] = new sphere(vec3(-8, 1, 0), 1.0, new metal(vec3(0.7, 0.6, 0.5), 0.0));*/

	/*hitable *list[6];
	list[0] = new sphere(vec3(0, 0, -1), 0.5, new lambertian(vec3(0.1,0.2,0.5)));
	list[1] = new sphere(vec3(0,-100.5, -1), 100, new lambertian(vec3(0.8, 0.8, 0.0)));
	list[2] = new sphere(vec3(1, 0, -1), 0.5, new metal(vec3(0.8, 0.6, 0.2), 0.3));
	list[3] = new sphere(vec3(-1, 0, -1), 0.5, new dielectric(1.5));
	list[4] = new sphere(vec3(-1, 0, -1), -0.45, new dielectric(1.5));*/

	/*
	float R = cos(3.1415927 / 4);
	list[5] = new sphere(vec3(-R, 0, -1), R, new lambertian(vec3(0, 0, 1)));
	list[6] = new sphere(vec3(R, 0, -1), R, new lambertian(vec3(1, 0, 0)));
	*/

	hitable *world = random_scene();
	//hitable *world = new hitable_list(list, 10);

	vec3 lookfrom(13, 2, 3);
	vec3 lookat(0,0,0);
	float dist_to_focus = 10.0;//(lookfrom - lookat).length();
	float aperture = 0.1;

	camera cam(lookfrom, lookat, vec3(0, 1, 0), 20, float(nx) / float(ny), aperture, dist_to_focus);

	for (int j = ny-1; j >= 0;j--) {
		for (int i = 0; i < nx;i++) {
			vec3 col(0, 0, 0);
			for (int s = 0; s < ns; s++) {
				float rnd1 = ((double)rand() / (RAND_MAX));
				float rnd2 = ((double)rand() / (RAND_MAX));
				float u = float(i + rnd1) / float(nx);
				float v = float(j + rnd2) / float(ny);
				ray r = cam.get_ray(u, v);
				vec3 p = r.point_at_parameter(2.0);
				col += color(r, world,0);
			}
			col /= float(ns);
			col = vec3(sqrt(col[0]), sqrt(col[1]), sqrt(col[2]));
			pixels.push_back((int)255.99*col[0]);
			pixels.push_back((int)255.99*col[1]);
			pixels.push_back((int)255.99*col[2]);
		}
	}

	//size, channels RGB, pointer to memory
	image_writer::save_png("out.png", nx, ny, 3, pixels.data());

	return 0;
}