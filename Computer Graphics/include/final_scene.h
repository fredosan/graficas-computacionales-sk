#pragma once

#include "scene.h"
#include <IL/il.h>

#include <vector>
#include "ifile.h"
#include "time.h"
#include "vec3.h"
#include "vec4.h"
#include "mat4.h"

#include <iostream>

//IrrKlang
#include <IK/irrKlang.h>

class final_scene : public scene {
public:
	//destructores
	~final_scene();

	void init();
	void awake();
	void sleep();
	void reset() {}
	void mainLoop();
	void resize(int width, int height);
	void normalKeysDown(unsigned char key);
	void normalKeysUp(unsigned char key) {}
	void specialKeys(int key) {}
	void passiveMotion(int x, int y) {}

	irrklang::ISoundEngine* engine = irrklang::createIrrKlangDevice();

private:
	static const int vaoSize = 3;
	//GLuint shader_program;
	//GLuint vao[vaoSize];
	//GLuint positionsVBO[vaoSize];
	//GLuint colorsVBO[vaoSize];
	//GLuint indicesBuffer[vaoSize];
	//
	////textures
	//ILuint imageID[vaoSize];
	//ILuint textureId[vaoSize];
	//
	//GLuint textureBuffer[vaoSize];

	GLuint shader_program;
	GLuint water_shader_program;
	GLuint vao[2];

	//terrain
	GLuint positionsVBO;
	GLuint normalsVBO;
	GLuint textureBuffer;

	//water
	GLuint waterVBO;
	GLuint waterNormalsVBO;
	GLuint colorsVBO;

	GLuint indicesBuffer;
	GLuint iWaterBuffer;

	//textures
	ILuint imageID;
	ILuint textureId;

	//helpers
	float swidth = 400.0f;
	float sheight = 400.0f;

	int indicesCount = 0;
	int iWaterCount = 0;

	float mz = 10.0f;
	float my = 0.0f;
	float mx = 0.0f;

	//movement & camera position
	std::vector<cgmath::vec2> chaikinCtr;
	std::vector<cgmath::vec2> cameraRoutePoints;

	float planetRot = 20.0f;

	float step = 0.1f;
	unsigned int animation = 1;

	int p[512];
};
