#include "scene_chaikin.h"
#include "vec2.h"
#include "mat3.h"
#include <math.h>

static cgmath::mat3 getForthPoints(cgmath::vec3 &pi, cgmath::vec3 &pii) {
	cgmath::vec3 direction = cgmath::vec3().normalize(pii -pi);
	cgmath::vec3 qi = (3.0 / 4.0)*pi + (1.0 / 4.0)*pii;
	cgmath::vec3 ri = (1.0 / 4.0)*pi + (3.0 / 4.0)*pii;
	return cgmath::mat3(qi,ri,direction);
}

static void loadVertex(std::vector<cgmath::vec2> &positions, int i, GLuint vao[], GLuint positionsVBO[]) {
	glGenVertexArrays(1, &vao[i]);
	glBindVertexArray(vao[i]);
	glGenBuffers(1, &positionsVBO[i]);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO[i]);
	glBufferData(GL_ARRAY_BUFFER, sizeof(cgmath::vec2) * positions.size(), positions.data(), GL_DYNAMIC_DRAW);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
	glBindBuffer(GL_ARRAY_BUFFER, positionsVBO[i]);
	glBindVertexArray(vao[i]);
}

static void getDashedPoints(std::vector<cgmath::vec2> &controlPoints, std::vector<cgmath::vec2> &dashedPoints, int loop) {
	for (std::vector<cgmath::vec2>::iterator it = controlPoints.begin(); it != controlPoints.end(); ++it) {
		cgmath::vec2 p1 = *it;
		cgmath::vec2 p2;
		if (*it == controlPoints.back()) {
			if (loop) {
				p2 = *controlPoints.begin();
			}
			else {
				p2 = p1;
			}
		}
		else {
			p2 = *std::next(it);
		}
		cgmath::vec2 v = p2 - p1;
		float lines = 100.0f;
		for (int j = 0; j < lines; j++) {
			cgmath::vec2 p3 = p1 + (j / lines) * v;
			dashedPoints.push_back(p3);
		}
	}
}

static void getCurvedPoints(std::vector<cgmath::vec2> &controlPoints, std::vector<cgmath::vec2> &curvedPoints, int ref, int loop) {
	std::vector<cgmath::vec2> subControlPoints = controlPoints;
	for (int i = 0; i < ref; i++) {
		cgmath::vec3 pi;
		cgmath::vec3 pii;

		std::vector<cgmath::vec2> tmpControlPoints;
		int onetime = 1;
		for (std::vector<cgmath::vec2>::iterator it = subControlPoints.begin(); it != subControlPoints.end(); ++it) {
			pi = cgmath::vec3(*it, 0.0);
			if (*it == subControlPoints.back()) {
				if (loop) {
					pii = cgmath::vec3(*subControlPoints.begin(), 0.0);
				}
				else {
					pii = pi;
				}
			}
			else {
				pii = cgmath::vec3(*std::next(it), 0.0);
			}
			cgmath::mat3 newMatrix = getForthPoints(pi, pii);
			if (onetime && !loop) {
				tmpControlPoints.push_back(cgmath::vec2(pi.x,pi.y));
				onetime = 0;
			}
			tmpControlPoints.push_back(cgmath::vec2(newMatrix[0].x, newMatrix[0].y));
			tmpControlPoints.push_back(cgmath::vec2(newMatrix[1].x, newMatrix[1].y));
		}
		subControlPoints.clear();
		subControlPoints = tmpControlPoints;
		tmpControlPoints.clear();

		if (onetime && !loop) {
			subControlPoints.erase(subControlPoints.begin());
			subControlPoints.pop_back();
		}
	}
	curvedPoints = subControlPoints;
}

static void draw(std::vector<cgmath::vec2> &dashed, std::vector<cgmath::vec2> &curved, GLuint vao[], GLuint positionsVBO[], GLuint positionsSize[], int objN) {
	for (int i = 0; i < 2; i++) {
		if (i % 2 == 0) {
			loadVertex(dashed, i + (objN * 2), vao, positionsVBO);
			positionsSize[i + (objN * 2)] = dashed.size();
		}
		else {
			loadVertex(curved, i + (objN * 2), vao, positionsVBO);
			positionsSize[i + (objN * 2)] = curved.size();
		}
	}
}

void scene_chaikin::init() {
	//std::vector<cgmath::vec2> controlPoints;
	std::vector<cgmath::vec2> dashedPoints;
	std::vector<cgmath::vec2> curvedPoints;

	std::vector<cgmath::vec2> leftEar = {
		cgmath::vec2(0.3f,0.75f),
		cgmath::vec2(0.25f,0.85f),
		cgmath::vec2(0.3f,0.95f),
		cgmath::vec2(0.4f,0.95f),
		cgmath::vec2(0.3f,0.85f),
		cgmath::vec2(0.33f,0.75f)
	};

	std::vector<cgmath::vec2> rightEar = {
		cgmath::vec2(leftEar[5].x + 0.25f,leftEar[5].y - 0.05f),
		cgmath::vec2(leftEar[4].x + 0.35f,leftEar[4].y - 0.07f),
		cgmath::vec2(leftEar[3].x + 0.3f,leftEar[3].y - 0.07f),
		cgmath::vec2(leftEar[2].x + 0.3f,leftEar[2].y - 0.02f),
		cgmath::vec2(leftEar[1].x + 0.35f,leftEar[1].y - 0.07f),
		cgmath::vec2(leftEar[0].x + 0.25f,leftEar[0].y - 0.0451f)
	};

	std::vector<cgmath::vec2> nose = {
		cgmath::vec2(0.25f,0.35f - 0.02f),
		cgmath::vec2(0.2375f - 0.025f,0.3625f + 0.01f),
		cgmath::vec2(0.2625f + 0.025f,0.3625f + 0.01f)
	};

	std::vector<cgmath::vec2> outLine = {//noloop
		cgmath::vec2(0.1f,0.4f),
		cgmath::vec2(0.15f,0.3f),
		cgmath::vec2(0.35f,0.32f),
		cgmath::vec2(0.4f,0.3f),
		cgmath::vec2(0.3f,0.05f),
		cgmath::vec2(0.5f,0.08f),
		cgmath::vec2(0.65f,0.05f),
		cgmath::vec2(0.55f,0.5f),
		cgmath::vec2(0.6f,0.65f),
		cgmath::vec2(0.6f,0.7f),
		cgmath::vec2(0.4f,0.75f),
		cgmath::vec2(0.2f,0.75f),
		cgmath::vec2(0.1f,0.65f)
	};

	std::vector<cgmath::vec2> leftLeg = {
		cgmath::vec2(0.35f,0.07f),
		cgmath::vec2(0.35f,-0.03f),
		cgmath::vec2(0.39f,-0.06f),
		cgmath::vec2(0.43f,-0.03f),
		cgmath::vec2(0.43f,0.07f)
	};

	std::vector<cgmath::vec2> rightLeg = {
		cgmath::vec2(0.35f + 0.17f,0.07f),
		cgmath::vec2(0.35f + 0.17f,-0.03f),
		cgmath::vec2(0.39f + 0.17f,-0.06f),
		cgmath::vec2(0.43f + 0.17f,-0.03f),
		cgmath::vec2(0.43f + 0.17f,0.07f)
	};

	std::vector<cgmath::vec2> armUnion = {
		cgmath::vec2(0.35f + 0.14f,0.2f + 0.07f),
		cgmath::vec2(0.43f + 0.13f,0.2f + 0.07f)
	};

	std::vector<cgmath::vec2> arm = {
		cgmath::vec2(0.35f + 0.14f,0.2f + 0.07f),
		cgmath::vec2(0.35f + 0.14f,0.2f + -0.03f),
		cgmath::vec2(0.39f + 0.13f,0.2f + -0.06f),
		cgmath::vec2(0.43f + 0.13f,0.2f + -0.03f),
		cgmath::vec2(0.43f + 0.13f,0.2f + 0.07f)
	};

	std::vector<cgmath::vec2> leftEye = {
		cgmath::vec2(0.0f,0.65f),
		cgmath::vec2(0.2f,0.65f),
		cgmath::vec2(0.2f,0.4f),
		cgmath::vec2(0.0f,0.4f)
	};

	std::vector<cgmath::vec2> rightEye = {
		cgmath::vec2(0.3f,0.65f),
		cgmath::vec2(0.5f,0.65f),
		cgmath::vec2(0.5f,0.4f),
		cgmath::vec2(0.3f,0.4f)
	};

	std::vector<cgmath::vec2> tongue = {
		cgmath::vec2(0.25f,0.3125f),
		cgmath::vec2(0.2f,0.25f),
		cgmath::vec2(0.35f,0.25f),
		cgmath::vec2(0.3f,0.3125f)
	};

	std::vector<cgmath::vec2> tongueLine = {
		cgmath::vec2(tongue[0].x + 0.025f,tongue[0].y + 0.01f),
		cgmath::vec2(tongue[0].x + 0.02f,tongue[0].y - 0.05f)
	};

	std::vector<cgmath::vec2> innerLeftEye = {
		cgmath::vec2(0.04f,0.49f),
		cgmath::vec2(0.06f,0.49f),
		cgmath::vec2(0.06f,0.47f),
		cgmath::vec2(0.04f,0.47f)
	};

	std::vector<cgmath::vec2> innerRightEye = {
		cgmath::vec2(innerLeftEye[0].x + 0.37f,innerLeftEye[0].y),
		cgmath::vec2(innerLeftEye[1].x + 0.37f,innerLeftEye[1].y),
		cgmath::vec2(innerLeftEye[2].x + 0.37f,innerLeftEye[2].y),
		cgmath::vec2(innerLeftEye[3].x + 0.37f,innerLeftEye[3].y)
	};

	std::vector<cgmath::vec2> downHead = {//noloop
		cgmath::vec2(-0.5f,-0.2f),
		cgmath::vec2(0.5f,-0.2f),
		cgmath::vec2(1.0f,-0.8f),
		cgmath::vec2(-1.0f,-0.8f)
	};

	//15
	const int objN = vaoSize/2;
	std::vector<cgmath::vec2> parts[objN] = {leftEar,rightEar,outLine,leftEye,rightEye,tongue,tongueLine,nose,downHead,innerLeftEye,innerRightEye,leftLeg,rightLeg,arm,armUnion};
	short int loopParts[objN] = {0,0,0,1,1,0,1,1,1,1,1,0,0,0,0};

	for (int i = 0; i < vaoSize; i++) {
		if (i % 2 == 0) {
			primitiveType[i] = GL_LINES;
		}
		else {
			if (!loopParts[i/2]) {
				primitiveType[i] = GL_LINE_STRIP;
			}
			else {
				primitiveType[i] = GL_LINE_LOOP;
			}
		}
	}

	for (int i = 0; i < objN; i++) {
		getDashedPoints(parts[i], dashedPoints, loopParts[i]);
		getCurvedPoints(parts[i], curvedPoints, ref, loopParts[i]);
		draw(dashedPoints, curvedPoints, vao, positionsVBO, positionsSize, i);
		dashedPoints.clear();
		curvedPoints.clear();
	}
}

void scene_chaikin::awake() {
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
	glColor3f(0.0f, 1.0f, 0.0f);
	glPointSize(0.0f);
}

void scene_chaikin::sleep() {
	glPointSize(0.0f);
}

void scene_chaikin::mainLoop() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	for (int i = 0; i < vaoSize; i++) {
		if (undraw) {
			if (i % 2 != 0) {
				glBindVertexArray(vao[i]);
				glDrawArrays(primitiveType[i], 0, positionsSize[i]);
				glBindVertexArray(vao[i]);
			}
		}
		else {
			glBindVertexArray(vao[i]);
			glDrawArrays(primitiveType[i], 0, positionsSize[i]);
			glBindVertexArray(vao[i]);
		}
	}
}

void scene_chaikin::normalKeysDown(unsigned char key) {
	GLenum mode[] = { GL_POINTS, GL_LINE_STRIP, GL_LINE_LOOP, GL_LINES, GL_LINE_STRIP_ADJACENCY, GL_LINES_ADJACENCY, GL_TRIANGLE_STRIP, GL_TRIANGLE_FAN, GL_TRIANGLES, GL_TRIANGLE_STRIP_ADJACENCY, GL_TRIANGLES_ADJACENCY
	};
	if (key == '1') {
		if(dashedOn){
			for (int i = 0; i < vaoSize; i+=2) {
				primitiveType[i] = mode[0];
				undraw = 1;
			}
			dashedOn = false;
		}
		else {
			for (int i = 0; i < vaoSize; i+=2) {
				primitiveType[i] = mode[3];
				undraw = 0;
			}
			dashedOn = true;
		}
	}
}

void passiveMotionPoint() {

}

void scene_chaikin::specialKeys(int key)
{
	//std::cout << "Point: " << pointAtMouse << "\n\n";
	////activate flag for passive motion
	////alt enter for example
	//controlPoints.push_back(pointAtMouse);
}

void scene_chaikin::passiveMotion(int x, int y)
{
	//pointAtMouse.x = cos(x);
	//pointAtMouse.y = sin(y);
}
